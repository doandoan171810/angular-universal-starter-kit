import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

import { Meta } from '@angular/platform-browser';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default,
})
export class HomeComponent implements OnInit {
  constructor(
    private metaTagService: Meta
  ) { }
  ngOnInit() {
    this.metaTagService.updateTag({
      property: 'og:title',
      content: "Thegioididong.com - Điện thoại, Laptop, Phụ kiện, Đồng hồ chính hãng"
    });
    this.metaTagService.updateTag({
      property: 'og:description',
      content: "Hệ thống bán lẻ điện thoại di động, smartphone, máy tính bảng, tablet, laptop, phụ kiện, smartwatch, đồng hồ chính hãng mới nhất, giá tốt, dịch vụ khách hàng được yêu thích nhất VN"
    });
    this.metaTagService.updateTag({
      property: 'og:image',
      content: "https://cdn.tgdd.vn/Products/Images/42/248283/Slider/samsung-galaxy-z-flip3-5g-256gb-230821-04105416.jpg"
    });
  }
}
