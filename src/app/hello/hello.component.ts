import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Meta } from '@angular/platform-browser';


@Component({
  selector: 'app-hello',
  templateUrl: './hello.component.html',
  styleUrls: ['./hello.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default,
})
export class HelloComponent implements OnInit {
  constructor(
    private metaTagService: Meta
  ) { }
  ngOnInit() {
    this.metaTagService.updateTag({
      property: 'og:title',
      content: "hello"
    });
    this.metaTagService.updateTag({
      property: 'og:description',
      content: "hello"
    });
    this.metaTagService.updateTag({
      property: 'og:image',
      content: "https://interactive-examples.mdn.mozilla.net/media/cc0-images/grapefruit-slice-332-332.jpg"
    });
  }
}
